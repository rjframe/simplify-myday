import vibe.vibe;

void main()
{
	auto router = new URLRouter;
	router.registerWebInterface(new WebInterface);
	router.get("*", serveStaticFiles("public"));

	auto settings = new HTTPServerSettings;
	settings.port = 8080;
	settings.sessionStore = new MemorySessionStore;
	settings.bindAddresses = ["::1", "127.0.0.1"];
	listenHTTP(settings, router);

	logInfo("Please open http://127.0.0.1:8080/ in your browser.");
	runApplication();
}

struct NavPage {
	string name;
	string path;
}

class WebInterface {
	struct Context {
		NavPage[] nav_pages;
		string active_page;
	}
	Context context;

	private SessionVar!(bool, "authenticated") is_authenticated;

	this() {
		context.nav_pages = new NavPage[](2);
		context.nav_pages ~= NavPage("Home", "/");
		context.nav_pages ~= NavPage("Contacts", "/contacts");
		context.active_page = "Home";
	}

	void index() {
		context.active_page = "Home";
		render!("index.dt", context);
	}

	void getContacts() {
		context.active_page = "Contacts";
		// TODO: Implement getContacts();
		redirect("/");
	}
}
