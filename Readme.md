The bootstrap project is located at https://bitbucket.org/simplifysystems/vibed-bootstrap - I have not yet placed it on the Dub registry.

This is largely a proof of concept to see if I can create a PIM that is at
least as functional as my paper organizer.
