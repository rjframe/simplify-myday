$( function() {
	$("#agenda").draggable({
		helper: "clone",
		revert: "invalid"
	});
	$("#agenda, #agenda ui").disableSelection();
});
